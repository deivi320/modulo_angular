import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/lista.model';

@Component({
  selector: 'app-destino',
  templateUrl: './destino.component.html',
  styleUrls: ['./destino.component.css']
})
export class DestinoComponent implements OnInit {
  @Input() destinos:DestinoViaje;
  @HostBinding('attr.class') cssclass='col-md-4';
  @Output() clicked:EventEmitter<DestinoViaje>;



  constructor() {
    this.clicked=new EventEmitter();
   }

  ngOnInit(): void {
  }
  ir(){
    this.clicked.emit(this.destinos);
    return false;
  }

}
